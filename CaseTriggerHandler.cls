public class CaseTriggerHandler 
{
    public static void preventCaseDeleteOnChildCaseNotClosed(Map<Id, Case> mapNewCases, Map<Id, Case> mapOldCases){
        List<Id> lstUpdateParentIds = new List<Id>();
        for(Id caseId : mapNewCases.keySet()){
            Case newCase = mapNewCases.get(caseId);
            Case oldCase = mapOldCases.get(caseId);
            if(newCase.Status == 'Closed' && oldCase.Status != 'Closed'){		// Is Updated Or Not
                lstUpdateParentIds.add(newCase.Id);
            }
        }
        if(lstUpdateParentIds.size() > 0){
            List<Case> lstChildCases = [SELECT Id, ParentID, Status FROM Case WHERE ParentId IN : lstUpdateParentIds];
            
            for(Case childCase : lstChildCases){
                if(childCase.Status != 'Closed'){
                    mapNewCases.get(childCase.ParentId).addError('Cannot Delete this Case, it has Child Case Not Closed');
            	}
        	}
        }
    }
}