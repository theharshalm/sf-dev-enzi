@isTest
public class TestCaseTrigger 
{
    @TestSetup
    static void testData(){
        List<Case> lstParentCases = new List<Case>();
        for(Integer index = 1; index <= 25; index++){
            Case parentCase = new Case();
            parentCase.Origin = 'Web';
            parentCase.Status = 'Working';
            parentCase.Subject = 'Parent Case ' +index;
            lstParentCases.add(parentCase); 
        }
        insert lstParentCases;
        
        List<Case> lstChildCases = new List<Case>();
        Integer index = 1;
        List<Case> lstNewParentCases = [SELECT Id FROM Case WHERE Subject LIKE 'Parent Case%'];
        for(Case parentCase : lstNewParentCases){
            Case childCase = new Case();
            childCase.Origin = 'Web';
            childCase.Status = 'New';
            childCase.Subject = 'Child Case ' + index++;
            childCase.ParentId = parentCase.Id;
            lstChildCases.add(childCase);
        }
        insert lstChildCases;       
    }
    
    @isTest		// Positive Test Case
    static void testpreventCaseDeleteOnChildCaseNotClosed(){
        Test.startTest();
        List<Case> lstToUpdateCases = new List<Case>();
        for(Case updateCase : [SELECT Id, Status, Subject FROM Case WHERE Subject LIKE 'Parent Case%']){
            updateCase.Status = 'Closed';
            lstToUpdateCases.add(updateCase);
        }
        try{
            update lstToUpdateCases;
        }catch(DMLException dml){
            System.debug(dml.getMessage());
            System.assert(dml.getMessage().contains('Cannot Delete this Case, it has Child Case Not Closed'));
        }
        Test.stopTest();
    }
    
    @isTest		// Negative Test Case
    static void testpreventCaseDeleteOnChildCaseNotClosedNegative(){
        Test.startTest();
        List<Case> lstToUpdateChildCases = new List<Case>();
        for(Case updateChildCase : [SELECT Id, Status, Subject FROM Case WHERE Subject LIKE 'Child Case%']){
            updateChildCase.Status = 'Closed';
            lstToUpdateChildCases.add(updateChildCase);
        }
        update lstToUpdateChildCases;
        
        List<Case> lstToUpdateParentCases = new List<Case>();
        for(Case updateCase : [SELECT Id, Status, Subject FROM Case WHERE Subject LIKE 'Parent Case%']){
            updateCase.Status = 'Closed';
            lstToUpdateParentCases.add(updateCase);
        }
        try{
            update lstToUpdateParentCases;
        }catch(DMLException dml){
            System.assert(false);
        }
        Test.stopTest();
    }
}