trigger CaseTrigger on Case (before update) 
{
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
        	CaseTriggerHandler.preventCaseDeleteOnChildCaseNotClosed(Trigger.newMap, Trigger.oldMap);
        }
    }

}